package com.yumaas.roya;


import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.base.models.Exam;

import java.util.ArrayList;


public class ExamsAdapter extends RecyclerView.Adapter<ExamsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Exam> exams;


    public ExamsAdapter(OnItemClickListener onItemClickListener, ArrayList<Exam> exams) {
        this.onItemClickListener = onItemClickListener;
        this.exams = exams;

    }


    @Override
    public int getItemCount() {
        return exams == null ? 0 : exams.size();
    }


    @Override
    public ExamsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exam, parent, false);
        ExamsAdapter.ViewHolder viewHolder = new ExamsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ExamsAdapter.ViewHolder holder, final int position) {
        

        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.name.setText(exams.get(position).name);

        holder.download.setOnClickListener(view -> {
            try{
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(exams.get(position).file));
                view.getContext().startActivity(browserIntent);
            }catch (Exception e){
                e.getStackTrace();
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        Button download;
        LinearLayout layout;
     
        public ViewHolder(View view) {
            super(view);
            name=view.findViewById(R.id.name);
            download=view.findViewById(R.id.download);
            layout = view.findViewById(R.id.layout);

        }
    }
}