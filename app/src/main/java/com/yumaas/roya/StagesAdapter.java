package com.yumaas.roya;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.roya.admin.ClassesFragment;
import com.yumaas.roya.base.models.Item;

import java.util.ArrayList;


public class StagesAdapter extends RecyclerView.Adapter<StagesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Item> items;


    public StagesAdapter(OnItemClickListener onItemClickListener, ArrayList<Item> items) {
        this.onItemClickListener = onItemClickListener;
        this.items = items;
    }


    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }


    @Override
    public StagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stage, parent, false);
        StagesAdapter.ViewHolder viewHolder = new StagesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final StagesAdapter.ViewHolder holder, final int position) {
        holder.name.setText(items.get(position).name);
        Picasso.with(holder.imageView.getContext()).load(items.get(position).image).into(holder.imageView);
        holder.itemView.setOnClickListener(view -> {
          onItemClickListener.onItemClickListener(position);
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            name=view.findViewById(R.id.name);

        }
    }
}