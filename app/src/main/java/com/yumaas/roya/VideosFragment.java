package com.yumaas.roya;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Video;
import com.yumaas.roya.teachers.FragmentAddVideo;
import java.util.ArrayList;


public class VideosFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_stages, container, false);

        Button button = rootView.findViewById(R.id.button);


        button.setText("اضافه فيديو");
        button.setVisibility(View.VISIBLE);

        if(getArguments().containsKey("teacher_id")) {
            button.setVisibility(View.GONE);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddVideo fragmentAddVideo = new FragmentAddVideo();
                FragmentHelper.addFragment(getActivity(),fragmentAddVideo,"FragmentAddVideo");
            }
        });


        ArrayList<Video> videos = DataBaseHelper.getDataLists().videos;

        final VideosAdapter StagesAdapter = new VideosAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, videos);


        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}