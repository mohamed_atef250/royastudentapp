package com.yumaas.roya.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.StagesAdapter;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.admin.ClassesFragment;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.user.UserClassesFragment;

import java.util.ArrayList;


public class StagesFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);

        rootView.findViewById(R.id.button).setVisibility(View.GONE);

        ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
        ArrayList<Item> itemsTemp = new ArrayList<>();


        for(int i=0; i<items.size(); i++){

            if(items.get(i).stageId==0  && items.get(i).classId==0){
                itemsTemp.add(items.get(i));
            }

        }


        final StagesAdapter StagesAdapter = new StagesAdapter(position -> {
            UserClassesFragment classesFragment = new UserClassesFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("id",itemsTemp.get(position).id);
            bundle.putInt("stage_id",itemsTemp.get(position).id);
            classesFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(getActivity(),classesFragment , "UserClassesFragment");

        }, itemsTemp);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}