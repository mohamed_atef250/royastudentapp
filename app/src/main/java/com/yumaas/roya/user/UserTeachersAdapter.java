package com.yumaas.roya.user;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class UserTeachersAdapter extends RecyclerView.Adapter<UserTeachersAdapter.ViewHolder> {
    OnItemClickListener onItemClickListener;
    ArrayList<User> users;


    public UserTeachersAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;

    }


    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }


    @Override
    public UserTeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher, parent, false);
        UserTeachersAdapter.ViewHolder viewHolder = new UserTeachersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserTeachersAdapter.ViewHolder holder, final int position) {


        ConnectionHelper.loadImage(holder.imageView,users.get(position).image);

        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.email.setText(users.get(position).email);
        holder.options.setVisibility(View.GONE);
        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));

        holder.itemView.setOnClickListener(view -> {
             onItemClickListener.onItemClickListener(position);
        });


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout layout,options;
        TextView name,phone,email;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            layout = view.findViewById(R.id.layout);
            options = view.findViewById(R.id.options);
            name=view.findViewById(R.id.name);
            phone=view.findViewById(R.id.title);
            email=view.findViewById(R.id.experiance);

        }
    }
}