package com.yumaas.roya.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.admin.ClaasesAdapter;
import com.yumaas.roya.admin.FragmentAddClasses;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Item;

import java.util.ArrayList;


public class UserClassesFragment extends Fragment {

    View rootView;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        button = rootView.findViewById(R.id.button);

        button.setVisibility(View.GONE);
        button.setText("اضافه فصل");

        int  x = getArguments().getInt("id");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddClasses addClasses =  new FragmentAddClasses();
                Bundle bundle = new Bundle();
                bundle.putInt("id",x);
                addClasses.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),addClasses,"FragmentAddStage");
            }
        });

        ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
        ArrayList<Item> itemsTemp = new ArrayList<>();


        for(int i=0; i<items.size(); i++){
            if(x==items.get(i).stageId){
                itemsTemp.add(items.get(i));
            }
        }

        final ClaasesAdapter StagesAdapter = new ClaasesAdapter(position -> {
            UserSubjectsFragment classesFragment = new UserSubjectsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("id",itemsTemp.get(position).id);
            bundle.putInt("class_id",itemsTemp.get(position).id);
            bundle.putInt("stage_id",getArguments().getInt("stage_id"));
            classesFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(getActivity(),classesFragment , "ClassesFragment");

        }, itemsTemp);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}