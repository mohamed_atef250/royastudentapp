package com.yumaas.roya.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.admin.FragmentAddSubject;
import com.yumaas.roya.admin.SubjectsAdapter;
import com.yumaas.roya.admin.SubjectsFragment;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Item;

import java.util.ArrayList;


public class UserSubjectsFragment extends Fragment {

    View rootView;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        button = rootView.findViewById(R.id.button);
        button.setVisibility(View.GONE);
        button.setText("اضافه ماده");
        int  x = getArguments().getInt("id");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("id",x);
                FragmentAddSubject fragmentAddStage =  new FragmentAddSubject();
                fragmentAddStage.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),fragmentAddStage,"FragmentAddStage");
            }
        });

        ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
        ArrayList<Item> itemsTemp = new ArrayList<>();


        for(int i=0; i<items.size(); i++){
            if(x==items.get(i).classId){
                itemsTemp.add(items.get(i));
            }
        }

        final SubjectsAdapter StagesAdapter = new SubjectsAdapter(position -> {
            TeachersFragment classesFragment = new TeachersFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("id",itemsTemp.get(position).id);
            bundle.putInt("class_id",getArguments().getInt("class_id"));
            bundle.putInt("stage_id",getArguments().getInt("stage_id"));
            bundle.putInt("subject_id",itemsTemp.get(position).id);

            classesFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(getActivity(), classesFragment, "ClassesFragment");
        }, itemsTemp);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVerticalGrid(getActivity(), programsList,2);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}