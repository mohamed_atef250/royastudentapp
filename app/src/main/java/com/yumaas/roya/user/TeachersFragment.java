package com.yumaas.roya.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.TeacherDetailsFragment;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.admin.AdminTeachersAdapter;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.User;

import java.util.ArrayList;


public class TeachersFragment extends Fragment {

    View rootView;

    Bundle bundle;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);

        rootView.findViewById(R.id.button).setVisibility(View.GONE);
        ArrayList<User>users = new ArrayList<>();
        ArrayList<User>tempUsers= DataBaseHelper.getDataLists().users;
        bundle = new Bundle();

        bundle.putInt("subject_id",getArguments().getInt("subject_id"));
        bundle.putInt("class_id",getArguments().getInt("class_id"));
        bundle.putInt("stage_id",getArguments().getInt("stage_id"));


        for(int i=0; i<tempUsers.size(); i++){
            if(tempUsers.get(i).type.equals("teacher")){
                users.add(tempUsers.get(i));
            }
        }


        final UserTeachersAdapter adminTeachersAdapter = new UserTeachersAdapter(position -> {
            TeacherDetailsFragment teeTeacherDetailsFragment = new TeacherDetailsFragment();
            bundle.putInt("teacher_id",users.get(position).id);
            teeTeacherDetailsFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(getActivity(),teeTeacherDetailsFragment , "UserClassesFragment");

        }, users);


        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(adminTeachersAdapter);

        return rootView;
    }
}