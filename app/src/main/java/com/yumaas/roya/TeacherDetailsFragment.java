package com.yumaas.roya;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Chat;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.teachers.LecturesFragment;
import com.yumaas.roya.user.UserChatFragment;
import com.yumaas.roya.volleyutils.ConnectionHelper;


public class TeacherDetailsFragment extends Fragment {

    View rootView;
    MaterialFavoriteButton materialFavoriteButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_teacher_details, container, false);

        materialFavoriteButton = rootView.findViewById(R.id.materialFavoriteButton);
        User savvedUser = DataBaseHelper.getSavedUser();

        if(getArguments().containsKey("teacher_id")) {


            rootView.findViewById(R.id.btn_job_appliers_reject).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserChatFragment userChatFragment =   new UserChatFragment();
                    Bundle bundle = new Bundle();
                    Chat chat = new Chat(DataBaseHelper.getSavedUser(),DataBaseHelper.findUser(getArguments().getInt("teacher_id")),"مرحبا بك اترك رسالتك");
                    chat.sender=getArguments().getInt("teacher_id");
                    bundle.putSerializable("chatItem",chat);
                    userChatFragment.setArguments(bundle);
                    FragmentHelper.addFragment(view.getContext(), userChatFragment, "ChatFragment");
                }
            });

            rootView.findViewById(R.id.btn_job_appliers_accept).setOnClickListener(view -> {


                Dialog rankDialog = new Dialog(view.getContext());
                rankDialog.setContentView(R.layout.rank_dialog);
                rankDialog.setCancelable(true);
                RatingBar ratingBar = rankDialog.findViewById(R.id.dialog_ratingbar);
                ratingBar.setRating(0);
                Button updateButton =  rankDialog.findViewById(R.id.rank_dialog_button);
                updateButton.setOnClickListener(v -> {
                    rankDialog.dismiss();
                    Toast.makeText(v.getContext(), "تم التقيم بنجاح", Toast.LENGTH_SHORT).show();
                });
                rankDialog.show();


            });

            User user = DataBaseHelper.findUser(getArguments().getInt("teacher_id"));

            materialFavoriteButton.setFavorite(DataBaseHelper.getSavedUser().checkFavourites(user.id));

            materialFavoriteButton.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
                @Override
                public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                    Toast.makeText(getActivity(), "تمت العمليه بنجاح", Toast.LENGTH_SHORT).show();
                    if(favorite){
                        savvedUser.addFavourites(user.id);
                    }else {
                        savvedUser.deleteFavourites(user.id);
                    }
                }
            });

            ImageView imageView;
            TextView name,phone,email;
            imageView = rootView.findViewById(R.id.imageView);
            name=rootView.findViewById(R.id.name);
            phone=rootView.findViewById(R.id.title);
            email=rootView.findViewById(R.id.experiance);


            ConnectionHelper.loadImage(imageView,user.image);
             name.setText(user.name);
            phone.setText(user.phone);
            email.setText(user.email);


        }else {
            rootView.findViewById(R.id.rl_main_container).setVisibility(View.GONE);
            rootView.findViewById(R.id.rl_action_bar).setVisibility(View.GONE);
        }

        Bundle bundle = new Bundle();

        if(getArguments()!=null)
        bundle=getArguments();

        bundle.putBoolean("show",true);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
               getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add("الفيدوهات", VideosFragment.class,bundle)
                .add("المحاضرات", LecturesFragment.class,bundle)
                .add("الامتحانات", ExamsFragment.class,bundle)
                .create());

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) rootView.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);





        return rootView;
    }
}