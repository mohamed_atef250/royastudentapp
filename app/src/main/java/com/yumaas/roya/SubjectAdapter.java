package com.yumaas.roya;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;


    public SubjectAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;

    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public SubjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subject, parent, false);
        SubjectAdapter.ViewHolder viewHolder = new SubjectAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SubjectAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.imageView.getContext()).load(images.get(position)).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(view.getContext(),MapActivity.class);
//                view.getContext().startActivity(intent);
                onItemClickListener.onItemClickListener(position);

            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;


        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);


        }
    }
}