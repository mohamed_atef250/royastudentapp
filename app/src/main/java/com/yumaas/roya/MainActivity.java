package com.yumaas.roya;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.yumaas.roya.user.FavouriteTeachersFragment;
import com.yumaas.roya.user.StagesFragment;
import com.yumaas.roya.user.TeachersFragment;

public class MainActivity extends AppCompatActivity {
    ResideMenu resideMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentHelper.replaceFragment(MainActivity.this, new StagesFragment(), "StagesFragment");

        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.img_blur);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

        // create menu items;
        String titles[] = {"الرئيسيه", "المعلمين المفضلين", "الملف الشخصي", "تسجيل خروج"};
        int icon[] = {R.drawable.ic_home, R.drawable.ic_teacher, R.drawable.ic_profile, R.drawable.ic_log_out};

        for (int i = 0; i < titles.length; i++) {
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            menuClick(item,i);
            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_RIGHT);
        }


        resideMenu.setMenuListener(new ResideMenu.OnMenuListener() {
            @Override
            public void openMenu() {

            }

            @Override
            public void closeMenu() {
            }
        });


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    private void menuClick(ResideMenuItem item, final int pos){
        item.setOnClickListener(view -> {
            if(pos==0){
                FragmentHelper.replaceFragment(MainActivity.this, new StagesFragment(), "StagesFragment");
            }else if(pos==1){
                FragmentHelper.replaceFragment(MainActivity.this, new FavouriteTeachersFragment(), "StagesFragment");
            }else if(pos==2){
                FragmentHelper.replaceFragment(MainActivity.this, new ProfileFragment(), "StagesFragment");

            }else if(pos==3){
                finish();

            }
            resideMenu.closeMenu();
        });
    }


}