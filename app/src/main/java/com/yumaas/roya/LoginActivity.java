package com.yumaas.roya;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.roya.admin.AdminMainActivity;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.SweetDialogs;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.teachers.TeacherMainActivity;

public class LoginActivity extends AppCompatActivity {
    EditText email,password;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},34442);

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(email.getText().toString().contains("admin")) {
                    Intent intent = new Intent(LoginActivity.this, AdminMainActivity.class);
                    startActivity(intent);
                }else if(email.getText().toString().contains("user")){

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }else {

                    User user = DataBaseHelper.loginUser(email.getText().toString()
                            , password.getText().toString());
                    if (user != null) {
                        DataBaseHelper.saveStudent(user);
                        Intent intent = new Intent(LoginActivity.this, TeacherMainActivity.class);

                        if(user.type.equals("user")){
                            intent = new Intent(LoginActivity.this, MainActivity.class);
                        }

                        startActivity(intent);


                        finish();
                    } else {
                        SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                    }



                }
            }
        });

        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });




    }


}