package com.yumaas.roya.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.StagesAdapter;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.user.UserSubjectsFragment;

import java.util.ArrayList;


public class ClassesFragment extends Fragment {

    View rootView;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        button = rootView.findViewById(R.id.button);


        button.setText("اضافه فصل");

        int  x = getArguments().getInt("id");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddClasses addClasses =  new FragmentAddClasses();
                Bundle bundle = new Bundle();
                bundle.putInt("id",x);
                addClasses.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),addClasses,"FragmentAddStage");
            }
        });

        ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
        ArrayList<Item> itemsTemp = new ArrayList<>();


        for(int i=0; i<items.size(); i++){
            if(x==items.get(i).stageId){
                itemsTemp.add(items.get(i));
            }
        }

        final ClaasesAdapter StagesAdapter = new ClaasesAdapter(position -> {
            SubjectsFragment classesFragment = new SubjectsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("id",itemsTemp.get(position).id);
            classesFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(getActivity(),classesFragment , "ClassesFragment");

        }, itemsTemp);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}