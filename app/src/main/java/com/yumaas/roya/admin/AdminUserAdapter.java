package com.yumaas.roya.admin;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.SweetDialogs;
import com.yumaas.roya.base.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AdminUserAdapter extends RecyclerView.Adapter<AdminUserAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<User> users;


    public AdminUserAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;

    }


    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }


    @Override
    public AdminUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_user, parent, false);
        AdminUserAdapter.ViewHolder viewHolder = new AdminUserAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminUserAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.imageView.getContext()).load(users.get(position).image).into(holder.imageView);

        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.email.setText(users.get(position).email);

        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));


        try {
            holder.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("هل تريد الحذف")
                            .setContentText("بالتاكيد حذف هذا المستخدم ؟")
                            .setConfirmText("نعم").setConfirmClickListener(sweetAlertDialog -> {
                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();

                        try {
                            sweetAlertDialog.cancel();
                            sweetAlertDialog.dismiss();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }

                    }).show();
                }
            });
        }catch (Exception e){
            e.getStackTrace();
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout layout;
        TextView name, phone, email;
        Button reject;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            layout = view.findViewById(R.id.layout);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            email = view.findViewById(R.id.email);
            reject = view.findViewById(R.id.reject);

        }
    }
}