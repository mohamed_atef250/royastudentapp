package com.yumaas.roya.admin;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;

public class AdminMainActivity extends AppCompatActivity {
    ResideMenu resideMenu;
    ImageView menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menu = findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resideMenu.isOpened()){
                    resideMenu.closeMenu();
                }else{
                    resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);

                }
            }
        });

        FragmentHelper.replaceFragment(AdminMainActivity.this, new AdminStagesFragment(), "UserAdminFragment");

        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.img_blur);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        // create menu items;
        String titles[] = {"الرئيسيه", "المعلمين" , "المراحل الدراسيه" , "تسجيل خروج"};
        int icon[] = {R.drawable.ic_home, R.drawable.ic_teacher, R.drawable.ic_profile, R.drawable.ic_log_out};

        for (int i = 0; i < titles.length; i++) {
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            menuClick(item,i);
            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_RIGHT);
        }


        resideMenu.setMenuListener(new ResideMenu.OnMenuListener() {
            @Override
            public void openMenu() {

            }

            @Override
            public void closeMenu() {
            }
        });

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    private void menuClick(ResideMenuItem item, final int pos){
        item.setOnClickListener(view -> {
            FragmentHelper.popAllFragments(AdminMainActivity.this);
            if(pos==0){
                FragmentHelper.replaceFragment(AdminMainActivity.this, new UserAdminFragment(), "UserAdminFragment");
            }else if(pos==1){
                FragmentHelper.replaceFragment(AdminMainActivity.this, new AdminTeachersFragment(), "AdminTeachersFragment");
            }else if(pos==2){
                FragmentHelper.replaceFragment(AdminMainActivity.this, new AdminStagesFragment(), "StagesFragment");
            }else if(pos==3){
                finish();
            }
            resideMenu.closeMenu();
        });
    }


}