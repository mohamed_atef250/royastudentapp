package com.yumaas.roya.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.Validate;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.SweetDialogs;
import com.yumaas.roya.base.filesutils.FileOperations;
import com.yumaas.roya.base.filesutils.VolleyFileObject;
import com.yumaas.roya.base.models.ImageResponse;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.volleyutils.ConnectionHelper;
import com.yumaas.roya.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;



public class FragmentAddStage extends Fragment {
    View rootView;
    EditText name,image;

    boolean isEdit=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_item, container, false);

        name=rootView.findViewById(R.id.name);

        image = rootView.findViewById(R.id.image);
        Button addd =  rootView.findViewById(R.id.btn);

        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });







        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                        DataBaseHelper.addStage(new Item(name.getText().toString(),selectedImage));
                        SweetDialogs.successMessage(requireActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                FragmentHelper.popAllFragments(getActivity());
                                FragmentHelper.replaceFragment(getActivity(), new AdminStagesFragment(), "AdminStagesFragment");

                            }
                        });




                }
            }
        });


        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())){
            name.setError("من فضلك املا الحقل");
            return false;
        }

        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);


        addServiceApi();


    }


    String selectedImage = "";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta", "" + selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



}
