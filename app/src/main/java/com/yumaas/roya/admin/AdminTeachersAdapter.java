package com.yumaas.roya.admin;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.TeacherDetailsActivity;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AdminTeachersAdapter extends RecyclerView.Adapter<AdminTeachersAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<User> users;


    public AdminTeachersAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;

    }


    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }


    @Override
    public AdminTeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_teacher, parent, false);
        AdminTeachersAdapter.ViewHolder viewHolder = new AdminTeachersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminTeachersAdapter.ViewHolder holder, final int position) {


        ConnectionHelper.loadImage(holder.imageView,users.get(position).image);

        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.email.setText(users.get(position).email);

        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));

        holder.itemView.setOnClickListener(view -> {
            onItemClickListener.onItemClickListener(position);
        });

        try {
            holder.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("هل تريد الحذف")
                            .setContentText("بالتاكيد حذف هذا المستخدم ؟")
                            .setConfirmText("نعم").setConfirmClickListener(sweetAlertDialog -> {
                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();

                        try {
                            sweetAlertDialog.cancel();
                            sweetAlertDialog.dismiss();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }

                    }).show();
                }
            });
        }catch (Exception e){
            e.getStackTrace();
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout layout;
        TextView name,phone,email;
        Button reject;
        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            layout = view.findViewById(R.id.layout);
            name=view.findViewById(R.id.name);
            phone=view.findViewById(R.id.phone);
            email=view.findViewById(R.id.email);
            reject = view.findViewById(R.id.btn_job_appliers_reject);
        }
    }
}