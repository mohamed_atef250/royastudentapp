package com.yumaas.roya.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.roya.R;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.User;

import java.util.ArrayList;


public class AdminTeachersFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);

        rootView.findViewById(R.id.button).setVisibility(View.GONE);
        ArrayList<User>users = new ArrayList<>();
        ArrayList<User>tempUsers= DataBaseHelper.getDataLists().users;

        for(int i=0; i<tempUsers.size(); i++){
            if(tempUsers.get(i).type.equals("teacher")){
                users.add(tempUsers.get(i));
            }
        }


        final AdminTeachersAdapter adminTeachersAdapter = new AdminTeachersAdapter(position -> {

        }, users);


        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(adminTeachersAdapter);


        return rootView;
    }
}