package com.yumaas.roya;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;


import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.PopUpItem;
import com.yumaas.roya.base.PopUpMenus;
import com.yumaas.roya.base.SweetDialogs;
import com.yumaas.roya.base.Validate;
import com.yumaas.roya.base.filesutils.FileOperations;
import com.yumaas.roya.base.filesutils.VolleyFileObject;
import com.yumaas.roya.base.models.ImageResponse;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.teachers.TeacherMainActivity;
import com.yumaas.roya.volleyutils.ConnectionHelper;
import com.yumaas.roya.volleyutils.ConnectionListener;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class RegisterActivity extends AppCompatActivity {
    EditText email,name,phone,password,type,image,userName;
    Button register;
    String selectedType;
    String selectedImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);
        email = findViewById(R.id.email);
        userName = findViewById(R.id.user_name);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        type = findViewById(R.id.type);
        password = findViewById(R.id.password);
        image=findViewById(R.id.image);
        register = findViewById(R.id.register);
        type.setFocusable(false);
        type.setClickable(true);

        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> names = new ArrayList<>();
                names.add("معلم");
                names.add("طالب");

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<names.size(); i++){
                    popUpMenus.add(new PopUpItem(i,names.get(i)));
                }
                PopUpMenus.showPopUp(RegisterActivity.this,type,popUpMenus).setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        type.setText(menuItem.getTitle());
                        if(menuItem.getItemId()==0){
                            selectedType="teacher";
                        }else {
                            selectedType="user";
                        }
                        return false;
                    }
                });
            }
        });

        register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(name.getText().toString(),
                        phone.getText().toString(),
                        email.getText().toString(),
                        password.getText().toString(),selectedImage,selectedType,userName.getText().toString());
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);
                SweetDialogs.successMessage(RegisterActivity.this, "تم التسجيل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);

                        if(selectedType.equals("teacher")){
                              intent = new Intent(RegisterActivity.this, TeacherMainActivity.class);

                        }
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }



    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(RegisterActivity.this, data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            image.setText("تم اختيار الصوره");
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }





    private boolean validate(){

        if(Validate.isEmpty(name.getText().toString())){
            name.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(email.getText().toString())){
            email.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(phone.getText().toString())){
            phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(password.getText().toString())){
            password.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(email.getText().toString())){
            email.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }


}
