package com.yumaas.roya.teachers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.ViewOperations;

import java.util.ArrayList;


public class FavouritesTeacherUserFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);


        ArrayList<String> images = new ArrayList<>();
        images.add("https://cdn.sabq.org/uploads/media-cache/resize_800_relative/uploads/material-file/601db3ecf1e326cff68b4582/601dc16e12ca7.jpg");
        images.add("https://nayrouz.com/assets/2019-11-06/news_gallery/132772_0_1573024594.jpg");
        images.add("https://gulfeyes.net/content/uploads/2021/02/06/d4198b4998.jpg");
        images.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYPIUsfoGCQHT1sTkFunmslW5M5LtuWG8e2Q&usqp=CAU");
        images.add("https://www.sayidy.net/sites/default/files/styles/660xauto/public/main/articles/united-arab-emirates-school.jpg");


        final TeacherUserAdapter adminUserAdapter = new TeacherUserAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, images);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(adminUserAdapter);


        return rootView;
    }
}