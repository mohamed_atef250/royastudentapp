package com.yumaas.roya.teachers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Chat;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class TeacherChatAdapter extends RecyclerView.Adapter<TeacherChatAdapter.ViewHolder> {

    private Context context;
    ArrayList<Chat> chattersList;

    User user;
    public TeacherChatAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList = chattersList;
        user = DataBaseHelper.getSavedUser();
    }


    @Override
    public TeacherChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        TeacherChatAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TeacherChatAdapter.ViewHolder holder, final int position) {


        if((user.id==chattersList.get(position).sender)) {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ConnectionHelper.loadImage(holder.image,chattersList.get(position).hadana.image);
        }else {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            ConnectionHelper.loadImage(holder.image,chattersList.get(position).user.image);
        }




        holder.message.setText(chattersList.get(position).message);
    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);

            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.message);
            image = itemView.findViewById(R.id.image);

        }
    }
}
