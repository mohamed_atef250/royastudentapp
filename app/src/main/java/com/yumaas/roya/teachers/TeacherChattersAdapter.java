package com.yumaas.roya.teachers;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.base.models.Chat;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class TeacherChattersAdapter extends RecyclerView.Adapter<TeacherChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;


    public TeacherChattersAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList=chattersList;
    }


    @Override
    public TeacherChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        TeacherChattersAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TeacherChattersAdapter.ViewHolder holder, final int position) {

        holder.message.setText(chattersList.get(position).user.name);


       ConnectionHelper.loadImage(holder.image , chattersList.get(position).user.image);


        holder.itemView.setOnClickListener(view -> {

            TeacherChatFragment userChatFragment =   new TeacherChatFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            userChatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), userChatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
