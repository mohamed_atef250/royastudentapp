package com.yumaas.roya.teachers;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.roya.ChatViewTestActivity;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;

import java.util.ArrayList;


public class TeacherUserAdapter extends RecyclerView.Adapter<TeacherUserAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;


    public TeacherUserAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;

    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public TeacherUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher_user, parent, false);
        TeacherUserAdapter.ViewHolder viewHolder = new TeacherUserAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final TeacherUserAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.imageView.getContext()).load(images.get(position)).into(holder.imageView);

        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ChatViewTestActivity.class);
                view.getContext().startActivity(intent);
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            layout = view.findViewById(R.id.layout);

        }
    }
}