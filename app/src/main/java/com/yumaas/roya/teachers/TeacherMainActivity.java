package com.yumaas.roya.teachers;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.ProfileFragment;
import com.yumaas.roya.R;

public class TeacherMainActivity extends AppCompatActivity {
    ResideMenu resideMenu;
    ImageView menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menu = findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resideMenu.isOpened()){
                    resideMenu.closeMenu();
                }else{
                    resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);

                }
            }
        });


        FragmentHelper.replaceFragment(TeacherMainActivity.this, new TeacherTeacherDetailsFragment(), "StagesFragment");

        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.img_blur);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

        // create menu items;
        String titles[] = {"الرئيسيه", "المحادثه",  "الملف الشخصي", "تسجيل خروج"};


        int icon[] = {R.drawable.ic_home,
                R.drawable.ic_chat,
                R.drawable.ic_profile,
                R.drawable.ic_log_out};

        for (int i = 0; i < titles.length; i++) {
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            menuClick(item,i);
            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_RIGHT);
        }


        resideMenu.setMenuListener(new ResideMenu.OnMenuListener() {
            @Override
            public void openMenu() {

            }

            @Override
            public void closeMenu() {
            }
        });


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    private void menuClick(ResideMenuItem item, final int pos){
        item.setOnClickListener(view -> {
            if(pos==0){
                FragmentHelper.replaceFragment(TeacherMainActivity.this, new TeacherTeacherDetailsFragment(), "StagesFragment");
            }else if(pos==1){
                FragmentHelper.replaceFragment(TeacherMainActivity.this, new TeacherChattersFragment(), "TeacherChattersFragment");
            }else if(pos==2){
                FragmentHelper.replaceFragment(TeacherMainActivity.this, new ProfileFragment(), "StagesFragment");
            }else {
                finish();
            }
            resideMenu.closeMenu();
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_main_container);
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }



}