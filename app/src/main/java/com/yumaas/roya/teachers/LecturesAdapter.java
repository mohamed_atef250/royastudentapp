package com.yumaas.roya.teachers;


import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.base.models.Files;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class LecturesAdapter extends RecyclerView.Adapter<LecturesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Files>books;


    public LecturesAdapter(OnItemClickListener onItemClickListener, ArrayList<Files> books) {
        this.onItemClickListener = onItemClickListener;
        this.books = books;

    }


    @Override
    public int getItemCount() {
        return books == null ? 0 : books.size();
    }


    @Override
    public LecturesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lecture, parent, false);
        LecturesAdapter.ViewHolder viewHolder = new LecturesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final LecturesAdapter.ViewHolder holder, final int position) {

        holder.name.setText(books.get(position).name);

        holder.layout.setBackgroundColor(holder.layout.getContext().getResources().getColor(position % 2 == 1 ? R.color.colorWhite : R.color.colorGrayLight));

        holder.download.setOnClickListener(view -> {
            try{
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(books.get(position).file));
                view.getContext().startActivity(browserIntent);
            }catch (Exception e){
                e.getStackTrace();
            }
        });

        holder.itemView.setOnClickListener(view -> {

        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layout;
        TextView name;
        Button download;

        public ViewHolder(View view) {
            super(view);
            layout = view.findViewById(R.id.layout);
            name = view.findViewById(R.id.name);
            download = view.findViewById(R.id.download);

        }
    }
}