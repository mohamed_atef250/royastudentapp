package com.yumaas.roya.teachers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.PopUpItem;
import com.yumaas.roya.base.PopUpMenus;
import com.yumaas.roya.base.filesutils.CompressObject;
import com.yumaas.roya.base.filesutils.FileOperations;
import com.yumaas.roya.base.filesutils.VolleyFileObject;
import com.yumaas.roya.base.models.Exam;
import com.yumaas.roya.base.models.ImageResponse;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.base.models.Video;
import com.yumaas.roya.volleyutils.ConnectionHelper;
import com.yumaas.roya.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class FragmentAddExam extends Fragment {
    private View rootView;
    private EditText bookLink,name,stage,classes,subject;
    private Button add;
    int stageId=0;
    int classId=0;
    int subjectId=0;
    String book="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_exam, container, false);

        bookLink = rootView.findViewById(R.id.bookLink);
        name = rootView.findViewById(R.id.name);
        add = rootView.findViewById(R.id.btn);
        stage   = rootView.findViewById(R.id.stage);
        classes = rootView.findViewById(R.id.classes);
        subject= rootView.findViewById(R.id.subject);


        bookLink.setFocusable(false);
        bookLink.setClickable(true);

        bookLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilePickerBuilder.getInstance()
                        .setMaxCount(1) //optional
                        .setActivityTheme(R.style.LibAppTheme) //optional
                        .pickFile(getActivity());
            }
        });


        add.setOnClickListener(view -> {
            Exam video= new Exam(book,name.getText().toString());
            video.classId=classId;
            video.subjectId=subjectId;
            video.stageId=stageId;
            video.teacherId=DataBaseHelper.getSavedUser().id;
            DataBaseHelper.addExam(video);
            Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
        });
        selectStages();
        selectClasses();
        selectSubjects();

        return rootView;
    }

    private void selectStages(){
        stage.setFocusable(false);
        stage.setClickable(true);
        stage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
                ArrayList<Item> itemsTemp = new ArrayList<>();

                for(int i=0; i<items.size(); i++){

                    if(items.get(i).stageId==0){
                        itemsTemp.add(items.get(i));
                    }

                }

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<itemsTemp.size(); i++){
                    popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
                }
                PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),stage,popUpMenus);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        stage.setText(menuItem.getTitle());

                            stageId = itemsTemp.get(menuItem.getItemId()).id;

                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }


    private void selectClasses(){
        classes.setFocusable(false);
        classes.setClickable(true);
        classes.setOnClickListener(view -> {

            ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
            ArrayList<Item> itemsTemp = new ArrayList<>();

            for(int i=0; i<items.size(); i++){

                if(items.get(i).stageId==stageId){
                    itemsTemp.add(items.get(i));
                }

            }

            ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
            for(int i=0; i<itemsTemp.size(); i++){
                popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
            }
            PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),classes,popUpMenus);

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    classes.setText(menuItem.getTitle());

                    classId = itemsTemp.get(menuItem.getItemId()).id;

                    return false;
                }
            });
            popupMenu.show();

        });
    }



    private void selectSubjects(){

        subject.setFocusable(false);
        subject.setClickable(true);
        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
                ArrayList<Item> itemsTemp = new ArrayList<>();

                for(int i=0; i<items.size(); i++){

                    if(items.get(i).classId==classId){
                        itemsTemp.add(items.get(i));
                    }

                }

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<itemsTemp.size(); i++){
                    popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
                }
                PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),classes,popUpMenus);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        subject.setText(menuItem.getTitle());

                        subjectId = itemsTemp.get(menuItem.getItemId()).id;

                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }



    ArrayList<VolleyFileObject> volleyFileObjects;
    List<Uri> docPaths;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        docPaths = new ArrayList<>();
        docPaths.addAll(data.getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                new VolleyFileObject("image",docPaths.get(0).getPath(),1);


        CompressObject compressObject = new CompressObject();
        String filePath = FileOperations.getPath(getActivity(), docPaths.get(0));
        compressObject.setBytes(FileOperations.fileToBytes(filePath));
        volleyFileObject.setCompressObject(compressObject);
        bookLink.setText("تم اضافه الامتحان");


        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }




    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;
                book = imageResponse.getState();
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }







}
