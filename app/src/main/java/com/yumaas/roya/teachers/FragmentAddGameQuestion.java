package com.yumaas.roya.teachers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.filesutils.FileOperations;
import com.yumaas.roya.base.filesutils.VolleyFileObject;
import com.yumaas.roya.base.models.ImageResponse;
import com.yumaas.roya.base.models.Question;
import com.yumaas.roya.volleyutils.ConnectionHelper;
import com.yumaas.roya.volleyutils.ConnectionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FragmentAddGameQuestion extends Fragment {
    private View rootView;
    private EditText firstAnswer, secondAnswer, thirdAnswer, forthAnswer, correctAnswer,question;
    private Button add;

    int selectedQuestion;
    private String img1,img2,img3,img4;
    private int correctAnswered=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_question, container, false);
        firstAnswer = rootView.findViewById(R.id.et_first_answer);
        secondAnswer = rootView.findViewById(R.id.et_second_answer);
        thirdAnswer = rootView.findViewById(R.id.et_third_answer);
        forthAnswer = rootView.findViewById(R.id.et_forth_answer);
        correctAnswer = rootView.findViewById(R.id.et_correct_answer);
        question = rootView.findViewById(R.id.question);

        add = rootView.findViewById(R.id.add);

        firstAnswer.setFocusable(false);
        secondAnswer.setFocusable(false);
        thirdAnswer.setFocusable(false);
        forthAnswer.setFocusable(false);
        correctAnswer.setFocusable(false);

        firstAnswer.setOnClickListener(view -> {
            selectedQuestion = 0;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });
        secondAnswer.setOnClickListener(view -> {
            selectedQuestion = 1;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });
        thirdAnswer.setOnClickListener(view -> {
            selectedQuestion = 2;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });

        forthAnswer.setOnClickListener(view -> {
            selectedQuestion = 3;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });

        add.setOnClickListener(view -> {
            DataBaseHelper.addQuestion(getArguments().getString("game_id"),new Question(DataBaseHelper.generateId()+"",question.getText().toString(),img1,img2,img3,img4,correctAnswered,getArguments().getString("game_id")));
            Toast.makeText(getActivity(), "تم اضافه السوال بنجاح", Toast.LENGTH_SHORT).show();
            FragmentHelper.popLastFragment(getActivity());
        });

        correctAnswer.setOnClickListener(view -> loadCitiesPopUp());




        return rootView;
    }


    private PopupMenu popUp = null;

    private void loadCitiesPopUp() {
        List<String> genderList = new ArrayList<>();


        genderList.add("الاختيار الاول");
        genderList.add("الاختيار الثاني");
        genderList.add("الاختيار الثالث");
        genderList.add("الاختيار الرابع");


        if (popUp == null) {
            popUp = new PopupMenu(getActivity(), correctAnswer);
            for (int i = 0; i < genderList.size(); i++) {
                popUp.getMenu().add(i, i, i, genderList.get(i));
            }


            popUp.setOnMenuItemClickListener(item -> {
                correctAnswer.setText(genderList.get(item.getItemId()));
                correctAnswered = item.getItemId();
                return false;
            });
        }
        popUp.show();
        popUp = null;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                //questionImage = imageResponse.getState();

                if (selectedQuestion == 0) {
                    img1 = imageResponse.getState();
                    firstAnswer.setText("تم اختيار الصوره");
                } else if (selectedQuestion == 1) {
                    img2 = imageResponse.getState();
                    secondAnswer.setText("تم اختيار الصوره");
                } else if (selectedQuestion == 2) {
                    img3 = imageResponse.getState();
                    thirdAnswer.setText("تم اختيار الصوره");
                } else if (selectedQuestion == 3) {
                    img4 = imageResponse.getState();
                    forthAnswer.setText("تم اختيار الصوره");
                }

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
