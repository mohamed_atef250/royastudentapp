package com.yumaas.roya.teachers;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.yumaas.roya.ChatViewTestActivity;
import com.yumaas.roya.ExamsFragment;
import com.yumaas.roya.R;
import com.yumaas.roya.VideosFragment;


public class TeacherTeacherDetailsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_teacher_details, container, false);


        rootView.findViewById(R.id.rl_main_container).setVisibility(View.GONE);
        rootView.findViewById(R.id.bottom).setVisibility(View.GONE);

        Bundle bundle = new Bundle();
        bundle.putBoolean("show",true);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
               getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add("الفيدوهات", VideosFragment.class,bundle)
                .add("المحاضرات", LecturesFragment.class,bundle)
                .add("الامتحانات", ExamsFragment.class,bundle)
                .create());

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) rootView.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


        rootView.findViewById(R.id.btn_job_appliers_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ChatViewTestActivity.class);
                view.getContext().startActivity(intent);
            }
        });


        return rootView;
    }
}