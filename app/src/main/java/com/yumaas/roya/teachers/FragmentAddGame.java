package com.yumaas.roya.teachers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.filesutils.FileOperations;
import com.yumaas.roya.base.filesutils.VolleyFileObject;
import com.yumaas.roya.base.models.Game;
import com.yumaas.roya.base.models.ImageResponse;
import com.yumaas.roya.volleyutils.ConnectionHelper;
import com.yumaas.roya.volleyutils.ConnectionListener;
import java.util.ArrayList;
import java.util.HashMap;


public class FragmentAddGame extends Fragment {
    private View rootView;
    private ImageView image;
    private EditText name;

    private String gameId = "";
    private String selectedImage = "";
    Game game;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_game, container, false);
        gameId = DataBaseHelper.generateId() + "";
        image = rootView.findViewById(R.id.image);
        name = rootView.findViewById(R.id.name);


        image.setOnClickListener(view -> {

            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });

        rootView.findViewById(R.id.add_question).setOnClickListener(view -> {
            if (name.getText() == null || name.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "من فضلك قم بكتابه اسم اللعبه", Toast.LENGTH_SHORT).show();
            } else {

                if (game == null) {
                    game = new Game(gameId, name.getText().toString(), selectedImage);


                    try {
                        if (getArguments().getBoolean("skills", false))
                            game.type = "skill";
                    } catch (Exception e) {
                        e.getStackTrace();
                    }

                    try {
                        if (getArguments().getBoolean("courses", false))
                            game.type = "courses";
                    } catch (Exception e) {
                        e.getStackTrace();
                    }

                    game.type = getArguments().getString("type");

                    DataBaseHelper.addGame(game);
                }


                FragmentAddGameQuestion addGameQuestion = new  FragmentAddGameQuestion();
                Bundle bundle = new Bundle();
                bundle.putString("game_id", gameId);
                addGameQuestion.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(), addGameQuestion, "FragmentAddGameQuestion");
            }
        });
        rootView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return rootView;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);


            volleyFileObjects.add(volleyFileObject);


            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image, selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
