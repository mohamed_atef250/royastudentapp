package com.yumaas.roya.teachers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.yumaas.roya.R;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.PopUpItem;
import com.yumaas.roya.base.PopUpMenus;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.base.models.Video;
import java.util.ArrayList;

public class FragmentAddVideo extends Fragment {
    private View rootView;
    private EditText youtube,name,stage,classes,subject;
    private Button add;
    int stageId=0;
    int classId=0;
    int subjectId=0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_video, container, false);

        youtube = rootView.findViewById(R.id.youtube);
        name = rootView.findViewById(R.id.name);
        add = rootView.findViewById(R.id.btn);
        stage   = rootView.findViewById(R.id.stage);
        classes = rootView.findViewById(R.id.classes);
        subject= rootView.findViewById(R.id.subject);


        add.setOnClickListener(view -> {
            Video video= new Video(youtube.getText().toString(),name.getText().toString());
            video.classId=classId;
            video.subjectId=subjectId;
            video.stageId=stageId;
            video.teacherId=DataBaseHelper.getSavedUser().id;
            DataBaseHelper.addVideo(video);
            Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
        });
        selectStages();
        selectClasses();
        selectSubjects();

        return rootView;
    }

    private void selectStages(){
        stage.setFocusable(false);
        stage.setClickable(true);
        stage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
                ArrayList<Item> itemsTemp = new ArrayList<>();

                for(int i=0; i<items.size(); i++){

                    if(items.get(i).stageId==0){
                        itemsTemp.add(items.get(i));
                    }

                }

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<itemsTemp.size(); i++){
                    popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
                }
                PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),stage,popUpMenus);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        stage.setText(menuItem.getTitle());

                            stageId = itemsTemp.get(menuItem.getItemId()).id;

                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }


    private void selectClasses(){
        classes.setFocusable(false);
        classes.setClickable(true);
        classes.setOnClickListener(view -> {

            ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
            ArrayList<Item> itemsTemp = new ArrayList<>();

            for(int i=0; i<items.size(); i++){

                if(items.get(i).stageId==stageId){
                    itemsTemp.add(items.get(i));
                }

            }

            ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
            for(int i=0; i<itemsTemp.size(); i++){
                popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
            }
            PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),classes,popUpMenus);

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    classes.setText(menuItem.getTitle());

                    classId = itemsTemp.get(menuItem.getItemId()).id;

                    return false;
                }
            });
            popupMenu.show();

        });
    }



    private void selectSubjects(){

        subject.setFocusable(false);
        subject.setClickable(true);
        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<Item> items = DataBaseHelper.getDataLists().stages;
                ArrayList<Item> itemsTemp = new ArrayList<>();

                for(int i=0; i<items.size(); i++){

                    if(items.get(i).classId==classId){
                        itemsTemp.add(items.get(i));
                    }

                }

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<itemsTemp.size(); i++){
                    popUpMenus.add(new PopUpItem(i,itemsTemp.get(i).name));
                }
                PopupMenu popupMenu = PopUpMenus.showPopUp(getActivity(),classes,popUpMenus);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        subject.setText(menuItem.getTitle());

                        subjectId = itemsTemp.get(menuItem.getItemId()).id;

                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }





}
