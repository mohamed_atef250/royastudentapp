package com.yumaas.roya.teachers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.FragmentAddFile;
import com.yumaas.roya.FragmentHelper;
import com.yumaas.roya.OnItemClickListener;
import com.yumaas.roya.R;
import com.yumaas.roya.ViewOperations;
import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Files;

import java.util.ArrayList;


public class LecturesFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);

        try {
            Button button = rootView.findViewById(R.id.button);

                button.setText("اضافه محاضره");
                button.setVisibility(View.VISIBLE);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentHelper.addFragment(getActivity(),new FragmentAddFile(),"FragmentAddFile");
                }
            });

            if(getArguments().containsKey("teacher_id")) {
                button.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.getStackTrace();
        }


        ArrayList<Files>books = DataBaseHelper.getDataLists().files;

        final LecturesAdapter teachersAdapter = new LecturesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, books);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(teachersAdapter);


        return rootView;
    }
}