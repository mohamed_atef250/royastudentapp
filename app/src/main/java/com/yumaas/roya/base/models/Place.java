package com.yumaas.roya.base.models;

import com.yumaas.roya.base.DataBaseHelper;


import java.io.Serializable;

public class Place implements Serializable {

    public int id;
    public String name,phone,image;


    public Place(String name, String phone, String image) {
        this.id = DataBaseHelper.generateId();
        this.name = name;
        this.phone=phone;
        this.image=image;


    }
}
