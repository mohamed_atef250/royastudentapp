package com.yumaas.roya.base.models;

import com.yumaas.roya.base.DataBaseHelper;

public class Item {
    public int id;
    public String name,image;
    public int stageId=0,classId=0;

    public Item(String name, String image){
        id= DataBaseHelper.generateId();
        this.name=name;
        this.image=image;
    }
}
