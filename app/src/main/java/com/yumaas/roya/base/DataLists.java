package com.yumaas.roya.base;

import com.yumaas.roya.base.models.Ads;
import com.yumaas.roya.base.models.Chat;
import com.yumaas.roya.base.models.Exam;
import com.yumaas.roya.base.models.Files;
import com.yumaas.roya.base.models.Game;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.base.models.Video;


import java.util.ArrayList;


public class DataLists {

    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Ads> ads = new ArrayList<>();
    public ArrayList<Item> stages = new ArrayList<>();
    public ArrayList<Item> classes = new ArrayList<>();
    public ArrayList<Item> subjects = new ArrayList<>();
    public ArrayList<Game> games= new ArrayList<>();
    public ArrayList<Video> videos= new ArrayList<>();
    public ArrayList<Exam> exams= new ArrayList<>();
    public ArrayList<Files> files= new ArrayList<>();
    public ArrayList<Chat> chats= new ArrayList<>();


}
