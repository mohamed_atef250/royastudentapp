package com.yumaas.roya.base;

import android.content.Context;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SweetDialogs {

    public static void successMessage(Context context, String message, SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText("تم بنجاح");
        sweetAlertDialog.setContentText(message).setConfirmButton("غلق", onSweetClickListener);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setCanceledOnTouchOutside(false);
        sweetAlertDialog.show();
    }

    public static void errorMessage(Context context, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("خطا");
        sweetAlertDialog.setContentText(message).setConfirmButton("غلق", sweetAlertDialog1 -> sweetAlertDialog1.cancel());
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setCanceledOnTouchOutside(false);
        sweetAlertDialog.show();
    }


}
