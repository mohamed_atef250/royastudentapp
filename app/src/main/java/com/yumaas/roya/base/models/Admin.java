package com.yumaas.roya.base.models;

import java.io.Serializable;

public class Admin implements Serializable {

    public String id,name;

    public Admin(String id, String name){
        this.id=id;
        this.name=name;
    }
}
