package com.yumaas.roya.base.models;

public class Comment {
    public String id, comment,postId;
    public User user;
    public Comment(String id, String comment, User user, String postId){
        this.comment = comment;
        this.id=id;
        this.user=user;
        this.postId=postId;
    }
}
