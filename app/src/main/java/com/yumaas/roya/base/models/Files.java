
package com.yumaas.roya.base.models;

import com.yumaas.roya.base.DataBaseHelper;

import java.io.Serializable;

public class Files implements Serializable {

    public String id,file,name;

    public int stageId=0,classId=0,subjectId=0,teacherId=0;


    public Files(String file, String name) {

        this.id = DataBaseHelper.generateId()+"";
        this.file = file;
        this.name=name;


    }
}
