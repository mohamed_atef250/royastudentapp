package com.yumaas.roya.base.models;

public class Expert {
    public String id,details,title;
    public User user;
    public Expert(String id, String title, String details, User user){
        this.title=title;
        this.details = details;
        this.id=id;
        this.user=user;
    }
}
