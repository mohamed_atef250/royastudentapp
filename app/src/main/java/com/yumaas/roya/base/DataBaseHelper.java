package com.yumaas.roya.base;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.yumaas.roya.base.models.Ads;
import com.yumaas.roya.base.models.Chat;
import com.yumaas.roya.base.models.Exam;
import com.yumaas.roya.base.models.Files;
import com.yumaas.roya.base.models.Game;
import com.yumaas.roya.base.models.Item;
import com.yumaas.roya.base.models.Question;
import com.yumaas.roya.base.models.User;

import com.yumaas.roya.base.models.Video;
import com.yumaas.roya.volleyutils.MyApplication;

import java.util.ArrayList;


public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance() {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }


    public static void addUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(student);
        dataLists.users = users;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }



    public static User loginUser(String userName,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if ((dataLists.users.get(i).email.equals(userName)||(dataLists.users.get(i).userName!=null&&dataLists.users.get(i).userName.equals(userName)))&&dataLists.users.get(i).password.equals(password)) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }

    public static User findUser(int id) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id == id ) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }


    public static void updateUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==student.id) {
                    dataLists.users.set(i,student);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeUser(User helper) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            dataLists.users = users;
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==helper.id) {
                    dataLists.users.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static int generateId() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        int id = getSharedPreferenceInstance().getInt("id",1);
        id++;
        prefsEditor.putInt("id", id);
        prefsEditor.apply();
        
        return id;
    }
    


    public static DataLists getDataLists() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("users", "");
        if (json.equals("")) return new DataLists();
        return gson.fromJson(json, DataLists.class);
    }


    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static void saveStudent(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(student);
        prefsEditor.putString("savedUser", json);
        prefsEditor.apply();
    }






    public static void addStage(Item ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Item> stages = dataLists.stages;
        if (stages == null) {
            stages = new ArrayList<>();
        }
        stages.add(ad);
        dataLists.stages = stages;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void addSubject(Item subject) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Item> subjects = dataLists.subjects;
        if (subjects == null) {
            subjects = new ArrayList<>();
        }
        subjects.add(subject);
        dataLists.subjects = subjects;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }

    public static void addClass(Item classes) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Item> classies = dataLists.classes;
        if (classies == null) {
            classies = new ArrayList<>();
        }
        classies.add(classes);
        dataLists.classes = classies;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void addAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads == null) {
            ads = new ArrayList<>();
        }
        ads.add(ad);
        dataLists.ads = ads;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void updateAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads != null) {

            dataLists.ads = ads;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.set(i,ad);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void addQuestion(String id, Question question) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Game> games = dataLists.games;
        if (games != null) {

            dataLists.games = games;

            for (int i = 0; i < dataLists.games.size(); i++) {
                if (dataLists.games.get(i).id.equals(id)) {
                    dataLists.games.get(i).addQuestion(question);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }



    public static void removeAd(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ad != null) {
            dataLists.ads = ads;
            for (int i = 0; i < dataLists.ads.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static void addGame(Game game) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Game> games = dataLists.games;
        if (games == null) {
            games = new ArrayList<>();
        }
        games.add(game);
        dataLists.games = games;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }
    public static void addVideo(Video video) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Video> videos = dataLists.videos;
        if (videos == null) {
            videos = new ArrayList<>();
        }
        videos.add(video);
        dataLists.videos = videos;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }



    public static void addExam(Exam exam) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Exam> exams = dataLists.exams;
        if (exams == null) {
            exams = new ArrayList<>();
        }
        exams.add(exam);
        dataLists.exams = exams;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addChat(Chat chat) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Chat> chats = dataLists.chats;
        if (chats == null) {
            chats = new ArrayList<>();
        }
        chats.add(chat);
        dataLists.chats = chats;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void addFile(Files file) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Files> files = dataLists.files;
        if (files == null) {
            files = new ArrayList<>();
        }
        files.add(file);
        dataLists.files = files;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }





}
