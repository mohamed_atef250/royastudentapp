package com.yumaas.roya.base.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Game implements Serializable {

    public String id, name,image,type="bags";
    public ArrayList<Question> questions;

    public Game(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image=image;
    }

    public void addQuestion(Question question) {
        if (questions == null) {
            questions = new ArrayList<>();
        }
        questions.add(question);
    }
}
