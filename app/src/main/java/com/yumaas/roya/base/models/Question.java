package com.yumaas.roya.base.models;

import java.io.Serializable;

public class Question implements Serializable {

    public String id,question, firstAnswer, secondAnswer, thirdAnswer, forthAnswer, gameId;
    public int correctAnswer;

    public Question(String id, String question, String firstAnswer, String secondAnswer, String thirdAnswer, String forthAnswer, int correctAnswer, String gameId) {
        this.id = id;
        this.firstAnswer = firstAnswer;
        this.secondAnswer = secondAnswer;
        this.thirdAnswer = thirdAnswer;
        this.forthAnswer = forthAnswer;
        this.correctAnswer = correctAnswer;
        this.gameId = gameId;
        this.question=question;
    }


}
