package com.yumaas.roya.base.models;

import android.util.Log;

import com.yumaas.roya.base.DataBaseHelper;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class User implements Serializable {
    public int id;
    public String name, image, phone, email, password,type,favourites="",userName="";


    public User(String name,  String phone, String email, String password, String image, String type,String userName) {
        this.id = DataBaseHelper.generateId();
        this.name = name;
        this.image = image;
        this.userName=userName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.type =type;
    }



    public void addFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();

        if(user.favourites!=null&&!user.favourites.equals(""))
            user.favourites+=","+id;
        else user.favourites+=id;
        DataBaseHelper.updateUser(user);
        DataBaseHelper.saveStudent(user);
    }

    public void deleteFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();
        String favs="";

        List<String>list = Arrays.asList(user.favourites.split(","));
        for(int i=0; i<list.size(); i++){
            Log.d("TAGOZ2",list.get(i));
            if(!list.get(i).equals(id+"")){

                if(!favs.equals("")) {
                    favs += "," + list.get(i);
                }
                else{
                    favs+=list.get(i);
                }

            }
        }

        user.favourites=favs;
        DataBaseHelper.updateUser(user);
        DataBaseHelper.saveStudent(user);
    }

    public boolean checkFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();

        if(user.favourites!=null&&!user.favourites.equals("")) {
            Log.d("TAGOZ",user.favourites);
            List<String>list = Arrays.asList(user.favourites.split(","));
            return list.contains(""+id);
        }
        else {
            return false;
        }


    }






}
