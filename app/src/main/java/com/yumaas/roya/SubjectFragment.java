package com.yumaas.roya;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class SubjectFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);


        ArrayList<String> images = new ArrayList<>();
        images.add("https://caus.org.lb/wp-content/uploads/2020/04/%D8%A7%D9%84%D9%84%D8%BA%D8%A9-%D8%A7%D9%84%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9.jpg");
        images.add("https://www.magltk.com/wp-content/uploads/2018/07/%D8%AA%D8%B9%D9%84%D9%85-%D8%A7%D9%84%D9%84%D8%BA%D8%A9-%D8%A7%D9%84%D8%A5%D9%86%D8%AC%D9%84%D9%8A%D8%B2%D9%8A%D8%A9-%D8%AA%D8%AD%D8%AF%D8%AB-%D9%88%D9%83%D8%AA%D8%A7%D8%A8%D8%A9-%D9%88%D9%82%D8%B1%D8%A7%D8%A1%D8%A9-%D8%A8%D8%A7%D8%AD%D8%AA%D8%B1%D8%A7%D9%81.jpg");
        images.add("https://spicy-news.com/wp-content/uploads/2019/06/%D8%A7%D9%84%D8%A3%D8%AE%D8%B7%D8%A7%D8%A1-%D8%A7%D9%84%D8%B4%D8%A7%D8%A6%D8%B9%D8%A9-%D9%81%D9%8A-%D8%A7%D9%84%D9%84%D8%BA%D8%A9-%D8%A7%D9%84%D9%81%D8%B1%D9%86%D8%B3%D9%8A%D8%A9-1025x610.jpg");
        images.add("https://www.ibelieveinsci.com/wp-content/uploads/f5ddf7bd97d01d87f4a7985398aea709_XL-1440x812.jpg");


        final SubjectAdapter StagesAdapter = new SubjectAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, images);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(StagesAdapter);


        return rootView;
    }
}