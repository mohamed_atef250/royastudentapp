package com.yumaas.roya;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.models.Exam;
import com.yumaas.roya.teachers.FragmentAddExam;

import java.util.ArrayList;


public class ExamsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stages, container, false);


        try {
            Button button = rootView.findViewById(R.id.button);

            button.setText("اضافه امتحان");
            button.setVisibility(View.VISIBLE);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentHelper.addFragment(getActivity(),new FragmentAddExam(),"FragmentAddExam");
                }
            });

            if(getArguments().containsKey("teacher_id")) {
                button.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.getStackTrace();
        }


        ArrayList<Exam> exams = DataBaseHelper.getDataLists().exams;



        final ExamsAdapter teachersAdapter = new ExamsAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, exams);


        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(teachersAdapter);


        return rootView;
    }
}