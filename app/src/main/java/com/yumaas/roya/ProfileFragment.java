package com.yumaas.roya;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.roya.base.DataBaseHelper;
import com.yumaas.roya.base.SweetDialogs;
import com.yumaas.roya.base.models.User;
import com.yumaas.roya.teachers.TeacherMainActivity;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ProfileFragment extends Fragment {
    EditText email, name, phone, password, userName;
    View rootView;
    Button register;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        email = rootView.findViewById(R.id.email);
        userName = rootView.findViewById(R.id.user_name);
        name = rootView.findViewById(R.id.name);
        phone = rootView.findViewById(R.id.phone);
        password = rootView.findViewById(R.id.password);
        register = rootView.findViewById(R.id.register);

        setDetails();

        register.setOnClickListener(view -> {

            User user = DataBaseHelper.getSavedUser();
            user.name = name.getText().toString();
            user.userName = userName.getText().toString();
            user.phone = phone.getText().toString();
            user.password = password.getText().toString();
            DataBaseHelper.updateUser(user);
            DataBaseHelper.saveStudent(user);
            SweetDialogs.successMessage(getActivity(), "تم تعديل البيانات بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    sweetAlertDialog.cancel();
                }
            });

        });

        return rootView;
    }


    private void setDetails() {
        User user = DataBaseHelper.getSavedUser();
        email.setText(user.email);
        userName.setText(user.userName);
        name.setText(user.name);
        phone.setText(user.phone);
        password.setText(user.password);
    }
}