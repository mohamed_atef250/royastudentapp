package com.yumaas.roya;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.roya.base.models.Video;
import com.yumaas.roya.volleyutils.ConnectionHelper;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Video>videos;


    public VideosAdapter(OnItemClickListener onItemClickListener, ArrayList<Video> videos) {
        this.onItemClickListener = onItemClickListener;
        this.videos = videos;

    }


    @Override
    public int getItemCount() {
        return videos == null ? 0 : videos.size();
    }


    @Override
    public VideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        VideosAdapter.ViewHolder viewHolder = new VideosAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final VideosAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.image, "https://img.youtube.com/vi/" +getYouTubeId(videos.get(position).video)+ "/hqdefault.jpg");
        holder.name.setText(videos.get(position).name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo(view.getContext(),getYouTubeId(videos.get(position).video));
            }
        });

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            image = view.findViewById(R.id.image);
        }
    }


    private String getYouTubeId(String youTubeUrl) {
        String pattern = "https?://(?:[0-9A-Z-]+\\.)?(?:youtu\\.be/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|</a>))[?=&+%\\w]*";

        Pattern compiledPattern = Pattern.compile(pattern,
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }
}