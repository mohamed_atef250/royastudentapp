package com.yumaas.roya;

import android.Manifest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shrikanthravi.chatview.data.Message;
import com.shrikanthravi.chatview.widget.ChatView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class ChatViewTestActivity extends AppCompatActivity {
    VideoView videoView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_view_test);

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
        //  volleyFileObjects = new ArrayList<>();
        chatView = findViewById(R.id.chatView);
        chatImage = findViewById(R.id.iv_chat_image);
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 100);

        Message message = new Message();
        message.setBody("hi how are you ");
        message.setType(Message.RightSimpleMessage);
        message.setUserName("Mohamed Atef");
        message.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message.setTime(getDate());
        chatView.addMessage(message);

        Message message2 = new Message();
        message2.setBody("Iam Fine Thank you");
        message2.setType(Message.LeftSimpleMessage);
        message2.setUserName("Ramy Adel");
        message2.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message2.setTime(getDate());
        chatView.addMessage(message2);


        message = new Message();
        message.setBody("I phone at my gym");
        message.setType(Message.RightSimpleMessage);
        message.setUserName("Mohamed Atef");
        message.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message.setTime(getDate());
        chatView.addMessage(message);

        message2 = new Message();
        message2.setBody("OMG How? they have cameras ?!");
        message2.setType(Message.LeftSimpleMessage);
        message2.setUserName("Ahmed Ali");
        message2.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message2.setTime(getDate());
        chatView.addMessage(message2);

        message = new Message();
        message.setBody("yes and i know who did it and i have image for him");
        message.setType(Message.RightSimpleMessage);
        message.setUserName("Mohamed Atef");
        message.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message.setTime(getDate());
        chatView.addMessage(message);


        message2 = new Message();
        message2.setBody("Nice send it please");
        message2.setType(Message.LeftSimpleMessage);
        message2.setUserName("Ahmed Ali");
        message2.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message2.setTime(getDate());
        chatView.addMessage(message2);


        ArrayList<Uri>images=new ArrayList<>();
        images.add(Uri.parse("https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80"));

        Message message3 = new Message();
        message3.setBody("Iam Fine Thank you");
        message3.setImageList(images);
        message3.setType(Message.RightSingleImage);
        message3.setUserName("Mohamed Atef");
        message3.setUserIcon(Uri.parse("https://icons-for-free.com/iconfiles/png/512/user-131964743686858060.png"));
        message3.setTime(getDate());
        chatView.addMessage(message3);

    }




    ChatView chatView;
    ImageView chatImage;
    boolean switchbool = true;
 

    private String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z", new Locale("en"));
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }


}
